define(['backbone'], function() {
	
	var routesMap = {
		'': 'app/app',
		'home':'js/app/helloworld',
			'*actions': 'defaultAction'
	};
	var Router = Backbone.Router.extend({
		 routes: routesMap,

		//路由初始化可以做一些事
		initialize: function() {},
		
		home:function(){
			console.log("home-----");
		},

		defaultAction: function () {
            console.log('404');
            location.hash = 'module2';
        }

	});

	var router = new Router();
	//彻底用on route接管路由的逻辑，这里route是路由对应的value
    router.on('route', function (route, params) {
        require([route], function (controller) {
//      	console.log('***** [{' + route +'}' +  params + '] *******');
//      	console.log('----- [{' + controller +'}' +  params + '] -----');
//          if(router.currentController && router.currentController !== controller){
//              router.currentController.onRouteChange && router.currentController.onRouteChange();
//          }
//          router.currentController = controller;
//          controller.apply(null, params);     //每个模块约定都返回controller
        });
    });
	return router; //这里必须的，让路由表执行
});