define(['lib','text!app/header.tpl'], function(LIB,tpl) {
		  var my_skins = [
    "skin-blue",
    "skin-black",
    "skin-red",
    "skin-yellow",
    "skin-purple",
    "skin-green",
    "skin-blue-light",
    "skin-black-light",
    "skin-red-light",
    "skin-yellow-light",
    "skin-purple-light",
    "skin-green-light"
  ];
	
	var Header = Backbone.Model.extend({
		//创建一个World的对象，拥有name属性
		title: null,
		url:null,
		film:null
	});
	var Headers = Backbone.Collection.extend({
		model:Header,
		url:"js/json/header.json",
		initialize: function(models, options) {

		},
		parse : function(data) {
        	var arrayObj = new Array(data.datas.length);
        	$.each(data.datas, function (i,item) {
        		var h = new Header();
        		h.title = item.title;
        		h.url = item.url;
        		h.film = item.film;
        		arrayObj[i]=h;
        	});
        	return arrayObj;
    	}
	});

	HeaderView = Backbone.View.extend({
		el: $(".main-header"),
		template: _.template(tpl),
		initialize: function() {			
			this.headers = new Headers(Header, {
				view: this
			})
			t = this;
			this.headers.fetch({success:function(collection, response){
				t.render();
			}});	
			
		},
		events: {
			//事件绑定，绑定Dom中id为check的元素
			"click #collapse":"collapse",
			"click #color-chooser > li": "skin"
		},
		render: function() {			
			datas = this.headers;
            this.$el.html(this.template(datas));
		},
		collapse: function() {
			if ($("body").hasClass('sidebar-collapse')) {
				$("body").removeClass('sidebar-collapse');
			} else {
				$("body").addClass('sidebar-collapse'); //.trigger('collapsed.pushMenu');
			}
			if ($("body").hasClass('sidebar-open')) {
				$("body").removeClass('sidebar-open').removeClass('sidebar-collapse'); //.trigger('collapsed.pushMenu');
			} else {
				$("body").addClass('sidebar-open'); //.trigger('expanded.pushMenu');
			}
		},
		skin: function(event) {
			var cls = $(event.target).attr('id');
			$.each(my_skins, function(i) {
				$("body").removeClass(my_skins[i]);
			});
			$("body").addClass(cls);
		}
	});
});