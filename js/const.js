define({
	STATUS:{
		SUCCESS:"0x0000",
		FAIL:"0x0001",
		SESSION_TIME_OUT: "0x0002",
	},
	MESSAGE:{
		ADD_TAB_PAGE:"add.tab.page",
	},
	ROUTE:{
		MENU_ACTION: "js/json/menu.json",
		DATA_ACTION: "js/json/data.json",
		LOGIN_ACTION: "passport/login.json",
		USER_ACTION: "user/query.json",
		USER_ADD_ACTION: "user/add.json",
		USER_GET_ACTION: "user/get.json",
		ROLE_ACTION: "role/query.json",
		ROLE_ADD_ACTION: "role/add.json",
		ROLE_GET_ACTION: "role/get.json",
		APPS_ACTION: "app/query.json",
		APPS_ADD_ACTION: "app/save.json",
		APPS_GET_ACTION: "app/get.json",
		NODE_ACTION: "node/query.json",
		NODE_ADD_ACTION: "node/save.json",
		NODE_GET_ACTION: "node/get.json",
		SHELL_ACTION: "shell/query.json",
		SHELL_ADD_ACTION: "shell/save.json",
		SHELL_GET_ACTION: "shell/get.json",
		TEST_ACTION: "unisize",		
		AUTH_ACTION: "js/json/auth.json",
		LOGIN_VIEW: "login.shtml",
	}
});